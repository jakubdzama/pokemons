val API_URL = js("getApiUrl()") as String

fun main() {
    println("Hello world!")
    val pokemonPresenter = PokemonPresenter()
    val pokemonPage = PokemonPage(presenter = pokemonPresenter, cardBuilder = CardBuilder())
    pokemonPage.show()
}