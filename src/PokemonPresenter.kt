import org.w3c.xhr.XMLHttpRequest

class PokemonPresenter : PokemonContract.Presenter {

    private lateinit var view: PokemonContract.View

    override fun attach(view: PokemonContract.View) {
        this.view = view
    }

    override fun loadPokemons() {
        view.showLoader()
        //2
        getAsync(API_URL) { response ->
            //3
            val pokemons = JSON.parse<Array<Pokemon>>(response)
            //4
            view.hideLoader()
            //5
            pokemons.forEach { pokemon -> println(pokemon.types) }
            view.showPokemons(pokemons.toList())
        }
    }

    private fun getAsync(url: String, callback: (String) -> Unit) {
        // 2
        val xmlHttp = XMLHttpRequest()
        // 3
        xmlHttp.open("GET", url)
        // 4
        xmlHttp.onload = {
            // 5
            if (xmlHttp.readyState == 4.toShort() && xmlHttp.status == 200.toShort()) {
                // 6
                callback.invoke(xmlHttp.responseText)
            }
        }
        // 7
        xmlHttp.send()
    }
}