import org.w3c.dom.*
import kotlin.browser.document
import kotlin.browser.window
import kotlin.dom.addClass

class CardBuilder {

    fun build(pokemon: Pokemon): HTMLElement {
        // 1
        val containerElement = document.createElement("div") as HTMLDivElement
        val imageElement = document.createElement("img") as HTMLImageElement
        val nameElement = document.createElement("div") as HTMLDivElement
        val dexElement = document.createElement("div") as HTMLDivElement
        val typesElement = document.createElement("ul") as HTMLUListElement
        val descriptionElement = document.createElement("div") as HTMLDivElement
        val viewDetailsButtonElement = document.createElement("button") as HTMLButtonElement

        // 2
        bind(containerElement = containerElement,
                pokemon = pokemon,
                imageElement = imageElement,
                nameElement = nameElement,
                dexElement = dexElement,
                typesElement = typesElement,
                descriptionElement = descriptionElement,
                viewDetailsButtonElement = viewDetailsButtonElement)

        // 3
        applyStyle(containerElement,
                imageElement = imageElement,
                nameElement = nameElement,
                dexElement = dexElement,
                typesElement = typesElement,
                descriptionElement = descriptionElement,
                viewDetailsButtonElement = viewDetailsButtonElement)

        containerElement
                .appendChild(
                        imageElement,
                        nameElement,
                        typesElement,
                        dexElement,
                        viewDetailsButtonElement
                )
        // 5
        return containerElement
    }

    private fun bind(containerElement: HTMLDivElement,
                     imageElement: HTMLImageElement,
                     nameElement: HTMLDivElement,
                     dexElement: HTMLDivElement,
                     descriptionElement: HTMLDivElement,
                     typesElement: HTMLUListElement,
                     viewDetailsButtonElement: HTMLButtonElement,
                     pokemon: Pokemon) {

        imageElement.src = pokemon.imageUrl
        nameElement.innerHTML = pokemon.name
        dexElement.innerHTML = pokemon.dex
        descriptionElement.innerHTML = pokemon.description
        bindPokemonTypes(typesElement, pokemon.types)
        viewDetailsButtonElement.innerHTML = "More"

        nameElement.addEventListener("click", {
            window.open(pokemon.url)
        })

        viewDetailsButtonElement.addEventListener("click", {
            if (viewDetailsButtonElement.innerHTML == "More") {
                viewDetailsButtonElement.innerHTML = "Less"
                containerElement.appendChild(descriptionElement)
            } else {
                viewDetailsButtonElement.innerHTML = "More"
                containerElement.removeChild(descriptionElement)
            }
        })
    }

    private fun bindPokemonTypes(typesElement: HTMLUListElement,
                                 types: Array<String>) {
        types.forEach {type ->
            val typeElement = document.createElement("li") as HTMLLIElement
            typeElement.innerHTML = type
            typesElement.appendChild(typeElement)
        }
    }

    private fun applyStyle(containerElement: HTMLDivElement,
                           imageElement: HTMLImageElement,
                           nameElement: HTMLDivElement,
                           dexElement: HTMLDivElement,
                           typesElement: HTMLUListElement,
                           descriptionElement: HTMLDivElement,
                           viewDetailsButtonElement: HTMLButtonElement) {
        containerElement.addClass("card", "card-shadow")
        imageElement.addClass("cover-image")
        nameElement.addClass("text-name", "float-left", "ripple")
        dexElement.addClass("text-dex", "float-left")
        typesElement.addClass("float-left")
        typesElement.children.asList().forEach { element ->
            element.addClass("text-type", "float-left")
        }
        descriptionElement.addClass("float-left", "text-description")
        viewDetailsButtonElement.addClass("view-details", "ripple", "float-right")
    }


    // 6
    private fun Element.appendChild(vararg elements: Element) {
        elements.forEach {
            this.appendChild(it)
        }
    }
}
