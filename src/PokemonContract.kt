interface PokemonContract {
    interface View {
        fun showPokemons(pokemons: List<Pokemon>) // Show a list of pokemons provided to it.
        fun showLoader() // Show a loading indicator while the app is fetching the book data from the server.
        fun hideLoader() // Hide the loading indicator.
    }

    interface Presenter {
        fun attach(view: View) // Display results on any view that it’s provided.
        fun loadPokemons() // Start loading the pokemon data from the data source. In this case, that’s a remote server.
    }
}
