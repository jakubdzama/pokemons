data class Pokemon(
        val name: String,
        val dex: String,
        val types: Array<String>,
        val description: String,
        val url: String,
        val imageUrl: String
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class.js != other::class.js) return false

        other as Pokemon

        if (name != other.name) return false
        if (dex != other.dex) return false
        if (!types.contentEquals(other.types)) return false
        if (description != other.description) return false
        if (url != other.url) return false
        if (imageUrl != other.imageUrl) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + dex.hashCode()
        result = 31 * result + types.contentHashCode()
        result = 31 * result + description.hashCode()
        result = 31 * result + url.hashCode()
        result = 31 * result + imageUrl.hashCode()
        return result
    }
}