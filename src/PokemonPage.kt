import org.w3c.dom.HTMLDivElement
import kotlin.browser.document

class PokemonPage(private val presenter: PokemonContract.Presenter, private val cardBuilder: CardBuilder) : PokemonContract.View {

    private val loader = document.getElementById("loader") as HTMLDivElement
    private val content = document.getElementById("content") as HTMLDivElement

    override fun showPokemons(pokemons: List<Pokemon>) {
        pokemons.forEach { pokemon ->
            val card = cardBuilder.build(pokemon)
            content.appendChild(card)
        }
    }

    override fun showLoader() {
        loader.style.visibility = "visible"
    }

    override fun hideLoader() {
        loader.style.visibility = "hidden"
    }

    fun show() {
        presenter.attach(this)
        presenter.loadPokemons()
    }
}